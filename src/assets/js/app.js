let scrollPos = window.scrollY;
let header;
let year = new Date().getFullYear();
window.addEventListener('scroll', () => {
	scrollPos = window.scrollY;
	if (scrollPos > 100) {
		addHeaderFixedToNav();
	} else {
		removeHeaderFixedToNav();
	}
});

function addHeaderFixedToNav() {
	header = document.querySelector('.nav').classList.add('nav--translateY');
}
function removeHeaderFixedToNav() {
	header = document.querySelector('.nav').classList.remove('nav--translateY');
}
window.onload = function() {
	document.body.className += " loaded";
	document.querySelector('.year').innerHTML = year.toString();
}
